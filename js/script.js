
'use strict';

 // When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
 window.onscroll = function() {scrollFunction()};

 function scrollFunction() {
       if (document.body.scrollTop > 108 || document.documentElement.scrollTop > 108) {
            document.getElementById("header-nav").style.padding = "0px";
        } else {
            document.getElementById("header-nav").style.padding = "22px 23px";
        }
  
    
 } 

(function ($) {


      // Add smooth scrolling to all links
  $(".pagelink a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

  

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
       
   /*  $('.service-request-form').on('submit',function(e){
        e.preventDefault();
        var name= $('#cname').val(),
            email = $('#cmail').val(),
            phone = $('#cphone').val();

        $.ajax({
                url: 'https://docs.google.com/forms/u/0/d/e/1FAIpQLSdMtuorBKOumJ6ZRNzeg737p75bWMcwFj4uLkgr-vSQpi9sBg/formResponse',
                type:'POST',
                data: {'entry.1302449621':name,'entry.1046093679':email,'entry.2113087789':phone}
            }); 
    });
 */ 

$('input[type=radio][name=host]').change(function() {
  if (this.value == 'yes') {
    $('.host-content').removeClass('d-none');
    $('#hours-needed, #host-needed').val('1');
  }
  else if (this.value == 'no') {
    $('.host-content').addClass('d-none');
  }
});

    $('.login-type').on('click',function(){
        $('.login-type').removeClass('active');
        $(this).addClass('active');
    });

    $('.register-type').on('click',function(){
        var type = $(this).data('type');
        $('.register-type').removeClass('active');
        $(this).addClass('active');
        if(type=='vendor'){
            $('.register-rider,.register-client').addClass('d-none');
            $('.register-vendor').removeClass('d-none');
            $('.page-title h2').text('Want to be part of our Network?');
            $('.description').text('If you need us to deliver for you to your client');
        }else if(type=='rider'){
            $('.register-vendor, .register-client').addClass('d-none');
            $('.register-rider').removeClass('d-none');
            $('.page-title h2').text('Want to drive for us? Work when you want.');
            $('.description').text('If you want to deliver from our platform');
        }else if(type=='client'){
          $('.register-rider,.register-vendor').addClass('d-none');
          $('.register-client').removeClass('d-none');
          $('.page-title h2').text('Want to be part of our Network?');
          $('.description').text('If you would like us to pick up from a vendor and deliver to you');
      }
    });

    /*------------------
        Background Set
    --------------------*/
   $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    /*------------------
		Navigation
	--------------------*/
    $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
    });

    /*------------------------
	 Slider
    ----------------------- */
    /*JS way for setting height: 100vh to slides' height*/
const $slides = $(".owl-carousel .owl-slide");
    $slides.css("height", $(window).height());
    $(window).resize(() => {
        $slides.css("height", $(window).height());
    });

$(".owl-carousel").on("initialized.owl.carousel", () => {
    setTimeout(() => {
      $(".owl-item.active .owl-slide-animated").addClass("is-transitioned");
      $("section").show();
    }, 200);
  });
  
  const $owlCarousel = $(".owl-carousel").owlCarousel({
    items: 1,
    loop: true,
    smartSpeed:1500,
    autoplay:true,
    autoplayTimeout:7500,
    autoplayHoverPause:false,
    nav: false});
  
  
  
  $owlCarousel.on("changed.owl.carousel", e => {
    $(".owl-slide-animated").removeClass("is-transitioned");
  
    const $currentOwlItem = $(".owl-item").eq(e.item.index);
    $currentOwlItem.find(".owl-slide-animated").addClass("is-transitioned");
  
    const $target = $currentOwlItem.find(".owl-slide-text");
    doDotsCalculations($target);
  });
  
  $owlCarousel.on("resize.owl.carousel", () => {
    setTimeout(() => {
      setOwlDotsPosition();
    }, 50);
  });
  
  /*if there isn't content underneath the carousel*/
  //$owlCarousel.trigger("refresh.owl.carousel");
  
  setOwlDotsPosition();
  
  function setOwlDotsPosition() {
    const $target = $(".owl-item.active .owl-slide-text");
    doDotsCalculations($target);
  }
  
  function doDotsCalculations(el) {
    const height = el.height();
    const { top, left } = el.position();
    const res = height + top + 20;
  
    $(".owl-carousel .owl-dots").css({
      top: `${res}px`,
      left: `${left}px` });
  
  }

    

})(jQuery);